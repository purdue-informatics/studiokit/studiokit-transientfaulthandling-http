using System.Net.Http;

namespace StudioKit.TransientFaultHandling.Http;

/// <summary>
/// A simple implementation of `IHttpClientFactory` for use only for scripting or testing.
/// </summary>
public class SimpleHttpClientFactory : IHttpClientFactory
{
	private static readonly HttpClientHandler Handler = new DecompressingHttpClientHandler();

	public HttpClient CreateClient(string name)
	{
		return new HttpClient(Handler, disposeHandler: false);
	}
}