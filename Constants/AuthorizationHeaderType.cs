﻿namespace StudioKit.TransientFaultHandling.Http.Constants;

public static class AuthorizationHeaderType
{
	public static string Basic = "Basic";

	public static string Bearer = "Bearer";
}