﻿using Microsoft.Extensions.Logging;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using StudioKit.Configuration;
using StudioKit.TransientFaultHandling.Http.ErrorDetectionStrategies;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.TransientFaultHandling.Http;

/// <summary>
/// This class is a wrapper for <see cref="HttpClient"/>. It will attempt to make an HTTP request with a
/// configurable amount of retries and timeout, as well as a fixed, one second retry interval.
/// </summary>
public class RetryingHttpClient : IHttpClient
{
	private readonly IHttpClientFactory _httpClientFactory;
	private readonly IRelease _release;
	private readonly ILogger<RetryingHttpClient> _logger;

	public RetryingHttpClient(
		IHttpClientFactory httpClientFactory,
		IRelease release,
		ILogger<RetryingHttpClient> logger = null,
		int retryCount = 2,
		int timeout = 30)
	{
		_httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
		_release = release ?? throw new ArgumentNullException(nameof(release));
		_logger = logger ?? throw new ArgumentNullException(nameof(logger));
		RetryCount = retryCount;
		Timeout = timeout;
	}

	public int RetryCount { get; set; }

	public int Timeout { get; set; }

	public async Task<HttpResponseMessage> GetAsync(
		Uri uri,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null,
		bool throwIfNotSuccessful = true)
	{
		async Task<HttpResponseMessage> SendRequestAsyncFunc(HttpClient client, CancellationToken c)
		{
			return await client.GetAsync(uri, c);
		}

		return await SendRequestAsync(uri, SendRequestAsyncFunc, cancellationToken, authorizationHeader, acceptHeaderValue,
			throwIfNotSuccessful);
	}

	public async Task<string> GetStringAsync(
		Uri uri,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null)
	{
		async Task<string> SendRequestAsyncFunc(HttpClient client, CancellationToken c)
		{
			return await client.GetStringAsync(uri, c);
		}

		return await SendRequestAsync(uri, SendRequestAsyncFunc, cancellationToken, authorizationHeader, acceptHeaderValue, false);
	}

	public async Task<byte[]> GetByteArrayAsync(
		Uri uri,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null)
	{
		async Task<byte[]> SendRequestAsyncFunc(HttpClient client, CancellationToken c)
		{
			return await client.GetByteArrayAsync(uri, c);
		}

		return await SendRequestAsync(uri, SendRequestAsyncFunc, cancellationToken, authorizationHeader, acceptHeaderValue, false);
	}

	public async Task<HttpResponseMessage> PostAsync(
		Uri uri,
		Func<Uri, HttpContent> getHttpContent,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null,
		bool throwIfNotSuccessful = true)
	{
		async Task<HttpResponseMessage> SendRequestAsyncFunc(HttpClient client, CancellationToken c)
		{
			var content = getHttpContent(uri);
			return await client.PostAsync(uri, content, c);
		}

		return await SendRequestAsync(uri, SendRequestAsyncFunc, cancellationToken, authorizationHeader, acceptHeaderValue,
			throwIfNotSuccessful);
	}

	public async Task<string> PostForStringAsync(
		Uri uri,
		Func<Uri, HttpContent> getHttpContent,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null,
		bool throwIfNotSuccessful = true)
	{
		var responseMessage = await PostAsync(uri, getHttpContent, cancellationToken, authorizationHeader, acceptHeaderValue,
			throwIfNotSuccessful);
		return await responseMessage.Content.ReadAsStringAsync(cancellationToken);
	}

	public async Task<HttpResponseMessage> PutAsync(
		Uri uri,
		Func<Uri, HttpContent> getHttpContent,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null,
		bool throwIfNotSuccessful = true)
	{
		async Task<HttpResponseMessage> SendRequestAsyncFunc(HttpClient client, CancellationToken c)
		{
			var content = getHttpContent(uri);
			return await client.PutAsync(uri, content, c);
		}

		return await SendRequestAsync(uri, SendRequestAsyncFunc, cancellationToken, authorizationHeader, acceptHeaderValue,
			throwIfNotSuccessful);
	}

	public async Task<HttpResponseMessage> SendAsync(
		HttpRequestMessage httpRequestMessage,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null,
		bool throwIfNotSuccessful = true)
	{
		async Task<HttpResponseMessage> SendRequestAsyncFunc(HttpClient client, CancellationToken c)
		{
			return await client.SendAsync(httpRequestMessage, c);
		}

		return await SendRequestAsync(httpRequestMessage.RequestUri, SendRequestAsyncFunc, cancellationToken, authorizationHeader,
			acceptHeaderValue, throwIfNotSuccessful);
	}

	#region Private Methods

	/// <summary>
	/// Decorator for the actual "send" logic which is provided by <paramref name="sendRequestAsyncFunc"/>.
	/// A delegate function is needed because the HttpClient is disposed of between retries, and all resources must be scoped inside of <paramref name="sendRequestAsyncFunc"/>.
	/// </summary>
	/// <typeparam name="T">The response type of <paramref name="sendRequestAsyncFunc"/>.</typeparam>
	/// <param name="uri">The URI of the request, used for logging.</param>
	/// <param name="sendRequestAsyncFunc">An async function that should call a specific method on the provided "client" function arg.</param>
	/// <param name="cancellationToken">(optional) A <see cref="CancellationToken"/> from the caller</param>
	/// <param name="authorizationHeader">(optional) Authorization Header </param>
	/// <param name="acceptHeaderValue">(optional) "Accepts" Header value</param>
	/// <param name="throwIfNotSuccessful">(optional) Decide whether to throw on http error code responses</param>
	/// <returns></returns>
	private async Task<T> SendRequestAsync<T>(
		Uri uri,
		Func<HttpClient, CancellationToken, Task<T>> sendRequestAsyncFunc,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null,
		bool throwIfNotSuccessful = true)
	{
		var guid = Guid.NewGuid().ToString();
		var messagePrefix = $"SendRequestAsync - {guid}";
		// do not include the query string in logs if it contains sensitive data, e.g. "code"
		var url = uri.AbsoluteUri.Contains("code=") ? uri.GetLeftPart(UriPartial.Path) : uri.AbsoluteUri;
		_logger.LogInformation($"{messagePrefix} - Start ({url})");

		var policy =
			new RetryPolicy<HttpTransientErrorDetectionStrategy>(new FixedInterval(RetryCount,
				TimeSpan.FromSeconds(1)));
		var executionCount = 0;
		T response = default;
		await policy.ExecuteAsync(async () =>
		{
			executionCount++;
			var executionMessagePrefix = $"{messagePrefix} - Execution {executionCount}";
			_logger.LogInformation(executionMessagePrefix);

			var client = _httpClientFactory.CreateClient();
			if (authorizationHeader != null)
				client.DefaultRequestHeaders.Authorization = authorizationHeader;
			if (acceptHeaderValue != null)
				client.DefaultRequestHeaders.Accept.Add(acceptHeaderValue);

			var header = new ProductHeaderValue(
				_release.ApplicationName,
				_release.Version
			);
			var userAgent = new ProductInfoHeaderValue(header);
			client.DefaultRequestHeaders.UserAgent.Add(userAgent);

			client.Timeout = TimeSpan.FromSeconds(Timeout);
			var startDateTime = DateTime.UtcNow;
			response = default;
			try
			{
				response = await sendRequestAsyncFunc(client, cancellationToken).ConfigureAwait(false);
				var elapsedSeconds = (DateTime.UtcNow - startDateTime).TotalSeconds;
				_logger.LogInformation($"{executionMessagePrefix} - Request finished after {elapsedSeconds} seconds");
			}
			catch (TaskCanceledException e)
			{
				var elapsedSeconds = (DateTime.UtcNow - startDateTime).TotalSeconds;
				if (elapsedSeconds >= Timeout)
				{
					_logger.LogInformation(
						$"{executionMessagePrefix} - TaskCanceledException - Request timed out after {elapsedSeconds} seconds");
					throw new HttpRequestException("Request timed out", e);
				}

				_logger.LogInformation(
					$"{executionMessagePrefix} - TaskCanceledException - Request cancelled after {elapsedSeconds} seconds");
				// always rethrow when request is cancelled. we want to just stop everything if this happens.
				// retry will not be triggered b/c we are not wrapping in a HttpRequestException
				throw;
			}

			// ensure that HTTP status codes of 500+ throw in order to cause a retry, if any more are possible
			// if there are no more retries, see below for the final throwIfNotSuccessful check
			if (executionCount <= RetryCount &&
				response is HttpResponseMessage innerResponseMessage &&
				innerResponseMessage.StatusCode >= HttpStatusCode.InternalServerError)
			{
				var responseBody = await innerResponseMessage.Content.ReadAsStringAsync(cancellationToken);
				if (!string.IsNullOrWhiteSpace(responseBody))
				{
					innerResponseMessage.ReasonPhrase = Regex.Replace(responseBody, @"\n|\r", string.Empty);
				}

				innerResponseMessage.EnsureSuccessStatusCode();
			}
		}, cancellationToken).ConfigureAwait(false);

		// ensure that unsuccessful HTTP status codes throw if requested
		if (throwIfNotSuccessful && response is HttpResponseMessage responseMessage)
		{
			// Check for a response body message if the HTTP status code is 400+
			if (responseMessage.StatusCode >= HttpStatusCode.BadRequest)
			{
				var responseBody = await responseMessage.Content.ReadAsStringAsync(cancellationToken);
				if (!string.IsNullOrWhiteSpace(responseBody))
				{
					responseMessage.ReasonPhrase = Regex.Replace(responseBody, @"\n|\r", string.Empty);
				}
			}

			responseMessage.EnsureSuccessStatusCode();
		}

		return response;
	}

	#endregion Private Methods
}