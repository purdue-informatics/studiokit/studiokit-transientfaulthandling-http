using System.Net;
using System.Net.Http;

namespace StudioKit.TransientFaultHandling.Http;

public class DecompressingHttpClientHandler : HttpClientHandler
{
	public DecompressingHttpClientHandler()
	{
		AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
	}
}