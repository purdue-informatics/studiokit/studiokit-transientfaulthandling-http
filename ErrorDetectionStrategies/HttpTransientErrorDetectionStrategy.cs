﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Net.Http;

namespace StudioKit.TransientFaultHandling.Http.ErrorDetectionStrategies;

public class HttpTransientErrorDetectionStrategy : ITransientErrorDetectionStrategy
{
	public bool IsTransient(Exception ex)
	{
		return ex is HttpRequestException;
	}
}