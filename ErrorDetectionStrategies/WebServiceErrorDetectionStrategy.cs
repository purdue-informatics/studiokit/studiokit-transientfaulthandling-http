﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Security;

namespace StudioKit.TransientFaultHandling.Http.ErrorDetectionStrategies;

public class WebServiceErrorDetectionStrategy : ITransientErrorDetectionStrategy
{
	public bool IsTransient(Exception ex)
	{
		// Catch normal HTTP errors
		if (ex is HttpRequestException)
			return true;

		// Catch 401 Unauthorized Challenge Response
		if (!(ex is MessageSecurityException))
			return false;

		var webException = ex.InnerException as WebException;
		if (webException?.Status != WebExceptionStatus.ProtocolError)
			return false;

		var response = webException.Response as HttpWebResponse;
		return response?.StatusCode == HttpStatusCode.Unauthorized;
	}
}