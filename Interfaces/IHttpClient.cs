﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.TransientFaultHandling.Http.Interfaces;

public interface IHttpClient
{
	/// <summary>
	/// Send a GET HTTP request. Returns the entire response object.
	/// </summary>
	/// <param name="uri">The endpoint URI</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <param name="authorizationHeader">(optional) Authorization Header </param>
	/// <param name="acceptHeaderValue">(optional) "Accepts" Header value</param>
	/// <param name="throwIfNotSuccessful">(optional) Whether or not to throw an exception on an error response</param>
	/// <returns>The entire response object</returns>
	Task<HttpResponseMessage> GetAsync(
		Uri uri,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null,
		bool throwIfNotSuccessful = true);

	/// <summary>
	/// Send a GET HTTP request. Returns the response body as a string.
	/// </summary>
	/// <param name="uri">The endpoint URI</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <param name="authorizationHeader">(optional) Authorization Header </param>
	/// <param name="acceptHeaderValue">(optional) "Accepts" Header value</param>
	/// <returns>The response body as a string</returns>
	Task<string> GetStringAsync(
		Uri uri,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null);

	/// <summary>
	/// Send a GET HTTP request. Returns the response body as a byte array.
	/// </summary>
	/// <param name="uri">The endpoint URI</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <param name="authorizationHeader">(optional) Authorization Header </param>
	/// <param name="acceptHeaderValue">(optional) "Accepts" Header value</param>
	/// <returns>The response body as a byte array</returns>
	Task<byte[]> GetByteArrayAsync(
		Uri uri,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null);

	/// <summary>
	/// Send a POST HTTP request. Must use a Func to generate the HttpContent per request/retry, otherwise it is Disposed between retries.
	/// </summary>
	/// <param name="uri">The endpoint URI</param>
	/// <param name="getHttpContent">A function to generate the request content. WARNING: This function must instantiate any resources within its own scope.</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <param name="authorizationHeader">(optional) Authorization Header </param>
	/// <param name="acceptHeaderValue">(optional) "Accepts" Header value</param>
	/// <param name="throwIfNotSuccessful">(optional) Decide whether to throw on http error code responses</param>
	/// <returns>The response message</returns>
	Task<HttpResponseMessage> PostAsync(
		Uri uri,
		Func<Uri, HttpContent> getHttpContent,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null,
		bool throwIfNotSuccessful = true);

	/// <summary>
	/// Send a POST HTTP request. Must use a Func to generate the HttpContent per request/retry, otherwise it is Disposed between retries.
	/// </summary>
	/// <param name="uri">The endpoint URI</param>
	/// <param name="getHttpContent">A function to generate the request content. WARNING: This function must instantiate any resources within its own scope.</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <param name="authorizationHeader">(optional) Authorization Header </param>
	/// <param name="acceptHeaderValue">(optional) "Accepts" Header value</param>
	/// <param name="throwIfNotSuccessful">(optional) Decide whether to throw on http error code responses</param>
	/// <returns>The response body as a string</returns>
	Task<string> PostForStringAsync(
		Uri uri,
		Func<Uri, HttpContent> getHttpContent,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null,
		bool throwIfNotSuccessful = true);

	/// <summary>
	/// Send a PUT HTTP request. Must use a Func to generate the HttpContent per request/retry, otherwise it is Disposed between retries.
	/// </summary>
	/// <param name="uri">The endpoint URI</param>
	/// <param name="getHttpContent">A function to generate the request content. WARNING: This function must instantiate any resources within its own scope.</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <param name="authorizationHeader">(optional) Authorization Header </param>
	/// <param name="acceptHeaderValue">(optional) "Accepts" Header value</param>
	/// <param name="throwIfNotSuccessful">(optional) Decide whether to throw on http error code responses</param>
	/// <returns>The response message</returns>
	Task<HttpResponseMessage> PutAsync(
		Uri uri,
		Func<Uri, HttpContent> getHttpContent,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null,
		bool throwIfNotSuccessful = true);

	/// <summary>
	/// Send a HTTP request message. Returns the entire response object.
	/// </summary>
	/// <param name="httpRequestMessage">The HttpRequestMessage to be sent</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <param name="authorizationHeader">(optional) Authorization Header </param>
	/// <param name="acceptHeaderValue">(optional) "Accepts" Header value</param>
	/// <param name="throwIfNotSuccessful">(optional) Decide whether to throw on http error code responses</param>
	/// <returns>The entire response object</returns>
	Task<HttpResponseMessage> SendAsync(
		HttpRequestMessage httpRequestMessage,
		CancellationToken cancellationToken = default,
		AuthenticationHeaderValue authorizationHeader = null,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null,
		bool throwIfNotSuccessful = true);

	int RetryCount { get; set; }

	int Timeout { get; set; }
}